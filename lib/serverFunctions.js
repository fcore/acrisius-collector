/**
 * Imports
 */
const request = require('request');
const winston = require('winston');

/**
 * Models
 */
const ServerResponse = require('../models/ServerResponse');

/**
 * Parse address and retrieve data from an game server
 * 
 * @param {String} address - Game Server's Address
 * @param {ObjectId} masterId - ID for a Master Server response
 */
const addServer = (address, masterId) => {
	winston.debug(`[GameServer] [${address}] Adding Server:`);

	const requestUrl = `http://${address}/`;

	request.get(requestUrl, (error, response, body) => {
		if (error) return winston.error(`[GameServer] [${address}] [Request] Error: ${error}`);
		winston.debug(`[GameServer] [${address}] [Request] Server Responded`);
		
		winston.silly(body)
		addServerResult(JSON.parse(body), address);
	});
};

/**
 * Adds a Game Server's reponse to MongoDB
 * 
 * @param {Object} data - Request Body from Request
 * @param {String} address - Game Server's address
 */
const addServerResult = (data, address) => {

	// Add the host to the document
	data.host = address.split(":")[0];

	// Define a Master Response Object
	const serverResponse = new ServerResponse(data);

	// Commit down to MongoDB
	serverResponse.save((error, document) => {
		if (error) {
			winston.error(`[GameServer] [${address}] [Mongo] There was an error saving the Game Server response.`);
			return winston.error(error);
		}

		winston.debug(`[GameServer] [${address}] Server Added`)
	});
};

/**
 * Exports
 */
module.exports.addServer = addServer;