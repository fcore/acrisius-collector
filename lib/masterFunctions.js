/**
 * Imports
 */
const request = require('request');
const winston = require('winston');
const addServer = require('./serverFunctions').addServer;

/**
 * Models
 */
const MasterResponse = require('../models/MasterResponse');

/**
 * Event Reciever from main event loop
 * 
 * @param {String} timer - Event Timing (Unutilized)
 */
const masterLoop = (timer, config) => {

	winston.debug("[Master] Retrieving Servers from Master Server");

	request.get(config.masterServer, (error, response, body) => {
		if (error) return winston.error(`[Master] [Request] Error: ${error}`);
		winston.debug(`[Master] [Request] Server Responded`);

		winston.silly(body);
		addMasterResult(JSON.parse(body));
	});
};

/**
 * Adds a Master Server's reponse to MongoDB
 * 
 * @param {Object} data - Request Body from Request
 */
const addMasterResult = (data) => {

	// Define a Master Response Object
	const masterResponse = new MasterResponse(data);

	// Commit down to MongoDB
	masterResponse.save((error, document) => {
		if (error) {
			winston.error("[Master] [Mongo] There was an error saving the Master Server response.");
			return winston.error(error);
		}

		winston.debug("[Master] Adding Servers")

		document.result.servers.forEach((serverAddress) => {
			addServer(serverAddress, document._id);
		})
	});
};

/**
 * Export Loop Function
 */
module.exports.masterLoop = masterLoop;