const mongoose = require('mongoose');

const masterSchema = new mongoose.Schema({
	listVersion: Number,
	result: {
		code: Number,
		msg: String,
		servers: [String]
	}
});

module.exports = mongoose.model("MasterServer", masterSchema);