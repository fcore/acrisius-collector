/**
 * Imports
 */
const mongoose = require('mongoose');
const argv = require('yargs').argv;
const winston = require('winston');
const EventedLoop = require('eventedloop')
const masterLoop = require('./lib/masterFunctions.js').masterLoop; 

/**
 * Configuration
 */
const config = {
	mongodb: argv.mongo || "mongodb://localhost/acrisius",
	logLevel: argv.log || "info",
	masterServer: argv.master || "http://158.69.166.144:8080/list"
};

/**
 * Configure Logging
 */
winston.level = config.logLevel;
winston.cli();
winston.info(`MongoDB URI: ${config.mongodb}`);
winston.debug("Config: ", config);

/**
 * Configure and connect to MongoDB
 */
mongoose.Promise = require('bluebird');
mongoose.connect(config.mongodb, {
	useMongoClient: true
}, () => { winston.info("Connected to MongoDB")});

/**
 * Timed Function
 */

const loop = new EventedLoop();
loop.every('1m', e => { masterLoop(e, config); });

/**
 * Start Timer
 */
loop.start();