# solidarity-collector
### Description:
The `solidarity-collector` is a worker to contact external servers and store responses in a MongoDB database.

## Setup
### Requirements:
* NodeJS: v6 or higher
* MongoDB: v3.4.4 or higher

### Instructions:
1. Clone the repo
2. Run `npm install`

## Starting the Service
The `solidarity-collector` has a few configurable command line options

* `--mongo` - MongoDB URI to connect to.
  * Default: `mongodb://localhost/solidarity`
* `--log` - Winston Log Level
  * Default: `info`
* `--master` - Master Server URL to connect to
  * Default: `http://158.69.166.144:8080/list`

Leaving the options at default, `node app.js` is sufficient.